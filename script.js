/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
//jquery sữ lý sự kiện khi load trang
$(document).ready(function(){
    "use strict";
    //Lấy dữ liệu dựa vào prop isPopular
    var vPopularObj= getPolularObjinData();
     //Hiển thị thông tin dũ liệu vào cùng polular
     displayDataInPopularSetion(vPopularObj);
     //Lấy dữ liệu dựa vào prop isTrending
    var vTrendingObj = getTrendingObjinData();
    //Hiển thị thông tin dũ liệu vào cùng trending
    displayDataInTrendingSetion(vTrendingObj);
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm displayDataInPopularSetion hiển thị thông tin dũ liệu vào cùng polular
function displayDataInTrendingSetion(paramData){
    "use strict";
   var vPopular = $(".trending");
   for (var bIndex = 0; bIndex < paramData.length; bIndex++) {
    var bTrendingCourses = paramData[bIndex];
    if(bTrendingCourses.courseCode == "FE_WEB_ANGULAR_101"){
        var vCard1 = vPopular.find(".1");
        vCard1.find(".rounded-top").attr("src",bTrendingCourses.coverImage);
        vCard1.find(".card-title").text(bTrendingCourses.courseName);
        vCard1.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bTrendingCourses.duration} ${bTrendingCourses.level}<br><b>${bTrendingCourses.discountPrice}$</b><del> ${bTrendingCourses.price}$</del>`
        );
        vCard1.find(".rounded-circle").attr("src",bTrendingCourses.teacherPhoto);
        vCard1.find(".pl-5").html(bTrendingCourses.teacherName)
        
    }
    if(bTrendingCourses.courseCode == "BE_WEB_PYTHON_301"){
        var vCard2 = vPopular.find(".2");
        vCard2.find(".rounded-top").attr("src",bTrendingCourses.coverImage);
        vCard2.find(".card-title").text(bTrendingCourses.courseName);
        vCard2.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bTrendingCourses.duration} ${bTrendingCourses.level}<br><b>${bTrendingCourses.discountPrice}$</b><del> ${bTrendingCourses.price}$</del>`
        );
        vCard2.find(".rounded-circle").attr("src",bTrendingCourses.teacherPhoto);
        vCard2.find(".pl-5").html(bTrendingCourses.teacherName)
        
    }
    if(bTrendingCourses.courseCode == "FE_WEB_JS_210"){
        var vCard3 = vPopular.find(".3");
        vCard3.find(".rounded-top").attr("src",bTrendingCourses.coverImage);
        vCard3.find(".card-title").text(bTrendingCourses.courseName);
        vCard3.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bTrendingCourses.duration} ${bTrendingCourses.level}<br><b>${bTrendingCourses.discountPrice}$</b><del> ${bTrendingCourses.price}$</del>`
        );
        vCard3.find(".rounded-circle").attr("src",bTrendingCourses.teacherPhoto);
        vCard3.find(".pl-5").html(bTrendingCourses.teacherName)
        
    }
    if(bTrendingCourses.courseCode == "FE_WEB_CSS_111"){
        var vCard4 = vPopular.find(".4");
        vCard4.find(".rounded-top").attr("src",bTrendingCourses.coverImage);
        vCard4.find(".card-title").text(bTrendingCourses.courseName);
        vCard4.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bTrendingCourses.duration} ${bTrendingCourses.level}<br><b>${bTrendingCourses.discountPrice}$</b><del> ${bTrendingCourses.price}$</del>`
        );
        vCard4.find(".rounded-circle").attr("src",bTrendingCourses.teacherPhoto);
        vCard4.find(".pl-5").html(bTrendingCourses.teacherName)
        
    }
    
    
   }
   vPopular.find("a:first-of-type").html();
}
//Hàm getPolularObjinData lấy dữ liệu dựa vào prop isPopular 
function getTrendingObjinData(){
    "use strict";
    var vTrendingObj = [];
    for (var bIndex = 0; bIndex < gCoursesDB.courses.length; bIndex++) {
        var bCourses = gCoursesDB.courses[bIndex];
        if(bCourses.isTrending == true){
            vTrendingObj.push(bCourses);
        }
    }
    console.log(vTrendingObj);
    return vTrendingObj;
}
//Hàm displayDataInPopularSetion hiển thị thông tin dũ liệu vào cùng polular
function displayDataInPopularSetion(paramData){
    "use strict";
   var vPopular = $(".popular");
   for (var bIndex = 0; bIndex < paramData.length; bIndex++) {
    var bPopularCourses = paramData[bIndex];
    if(bPopularCourses.courseCode == "FE_WEB_GRAPHQL_104"){
        var vCard1 = vPopular.find(".1");
        vCard1.find(".rounded-top").attr("src",bPopularCourses.coverImage);
        vCard1.find(".card-title").text(bPopularCourses.courseName);
        vCard1.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bPopularCourses.duration} ${bPopularCourses.level}<br><b>${bPopularCourses.discountPrice}$</b><del> ${bPopularCourses.price}$</del>`
        );
        vCard1.find(".rounded-circle").attr("src",bPopularCourses.teacherPhoto);
        vCard1.find(".pl-5").html(bPopularCourses.teacherName)
        
    }
    if(bPopularCourses.courseCode == "FE_WEB_JS_210"){
        var vCard2 = vPopular.find(".2");
        vCard2.find(".rounded-top").attr("src",bPopularCourses.coverImage);
        vCard2.find(".card-title").text(bPopularCourses.courseName);
        vCard2.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bPopularCourses.duration} ${bPopularCourses.level}<br><b>${bPopularCourses.discountPrice}$</b><del> ${bPopularCourses.price}$</del>`
        );
        vCard2.find(".rounded-circle").attr("src",bPopularCourses.teacherPhoto);
        vCard2.find(".pl-5").html(bPopularCourses.teacherName)
        
    }
    if(bPopularCourses.courseCode == "FE_WEB_CSS_111"){
        var vCard3 = vPopular.find(".3");
        vCard3.find(".rounded-top").attr("src",bPopularCourses.coverImage);
        vCard3.find(".card-title").text(bPopularCourses.courseName);
        vCard3.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bPopularCourses.duration} ${bPopularCourses.level}<br><b>${bPopularCourses.discountPrice}$</b><del> ${bPopularCourses.price}$</del>`
        );
        vCard3.find(".rounded-circle").attr("src",bPopularCourses.teacherPhoto);
        vCard3.find(".pl-5").html(bPopularCourses.teacherName)
        
    }
    if(bPopularCourses.courseCode == "FE_WEB_WORDPRESS_111"){
        var vCard4 = vPopular.find(".4");
        vCard4.find(".rounded-top").attr("src",bPopularCourses.coverImage);
        vCard4.find(".card-title").text(bPopularCourses.courseName);
        vCard4.find(".card-text").html("").append(
            `<i class="far fa-clock"></i> ${bPopularCourses.duration} ${bPopularCourses.level}<br><b>${bPopularCourses.discountPrice}$</b><del> ${bPopularCourses.price}$</del>`
        );
        vCard4.find(".rounded-circle").attr("src",bPopularCourses.teacherPhoto);
        vCard4.find(".pl-5").html(bPopularCourses.teacherName)
        
    }
    
    
   }
   vPopular.find("a:first-of-type").html();
}
//Hàm getPolularObjinData lấy dữ liệu dựa vào prop isPopular 
function getPolularObjinData(){
    "use strict";
    var vPopularObj = [];
    for (var bIndex = 0; bIndex < gCoursesDB.courses.length; bIndex++) {
        var bCourses = gCoursesDB.courses[bIndex];
        if(bCourses.isPopular == true){
           vPopularObj.push(bCourses);
        }
    }
    console.log(vPopularObj);
    return vPopularObj;
}
// hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
function getNextId() {
    var vNextId = 0;
    // Nếu mảng chưa có đối tượng nào thì Id = 1
    if(gCoursesDB.courses.length == 0) {
      vNextId = 1;
    }
  // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
else {
      vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
    }
    return vNextId;
  }
